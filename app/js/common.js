$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
      items:1,
      autoplay: true,
      loop:true,
      mouseDrag: false,
      nav:true,
      navText: ['left', 'right']
  });
});
