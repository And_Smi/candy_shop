var gulp = require("gulp"), // подключение галпа
	less = require("gulp-less"), // подключение конвертера less
	sync = require("browser-sync"), // подключение livereload
  uglify = require("gulp-uglifyjs"), // подключение сжатия js
  concat = require("gulp-concat"), // подключение плагина объединения файлов
    nano = require("gulp-cssnano"), // подключение сжатия css
  rename = require("gulp-rename"), // подключение плагина переименовывания файлов
     del = require("del"), // подключение плагина удаления файлов
imageMin = require("gulp-imagemin"), // подключение плагина сжатия фото
pngQuant = require("imagemin-pngquant"), // подключение плагина сохранения качества фото
   cache = require("gulp-cache"), // подключение плагина для кеширования
  prefix = require("gulp-autoprefixer"); // подключение плагина для выставления префиксов свойствам css

  /************************************************
  Создание таска для конвертации Less-файлов в css
  1. Выборка файлов Less
  2. Конвертация Less
  3. Добавление префиксов к css-свойствам
  4. Отправка в нужную папку css-файлов
  *************************************************/
  gulp.task("less", function(){
  	return gulp.src(["app/less/**/*.less", "!app/less/**/_*.less"])
  	.pipe(less())
  	.pipe(prefix([
  		'last 20 versions',
  		'>1%',
  		'ie 8',
  		'ie 7'
  	],
  	{
  		cascade: true // красота отображения стилей
  	}
  	))
  	.pipe(gulp.dest("app/presets/css"));
  });

  /****************************************
  Создание таска для JS скриптов библиотек
  1. Выборка JS-библиотек
  2. Объединяем все файлы библиотек в один
  3. Сжимаем объединенный файл
  4. Отправка в нужную папку js-файла
  *****************************************/
  gulp.task("scripts", function(){
  	return gulp.src([
  		'app/libs/jquery/dist/jquery.min.js',
  		'app/libs/bootstrap/dist/js/bootstrap.min.js',
          'app/libs/owl.carousel/dist/owl.carousel.min.js'
  	])
  	.pipe(concat("libs.min.js"))
  	.pipe(uglify())
  	.pipe(gulp.dest("app/js"));
  });

  /******************************************
  Создание таска для фото
  1. Выборка всех фото
  2. Кеширование всех фото и их оптимизация
  3. Отправка всех фото на продакшен
  *******************************************/
  gulp.task("img", function(){
  	return gulp.src("app/img/**/*")
  	.pipe(cache(imageMin({
  		interlaced: true,
  		progressive: true,
  		svgoPlugins: [{removeViewBox: false}],
  		use: [pngQuant()]
  	})))
  	.pipe(gulp.dest("dist/img"));
  });

  /****************************************************
  Создание таска для оптимизации стилей css-файлов
  1. Выборка css
  2. Сжатие всех css-файлов
  3. Объединение всех css-файлов
  4. Отправка файла в папку css
  *****************************************************/
  gulp.task("styles",  function(){
  	return gulp.src([
  		"app/presets/css/reset.css",
  		"app/presets/css/styles.css"
  	])
  	.pipe(nano())
  	.pipe(concat("all.min.css"))
  	.pipe(gulp.dest("dist/css"));
  });

  /************************************
  Создание таска для удаления файлов
  1. Удаление папки dist
  *************************************/
  gulp.task("clean", function(){
  	return del.sync('dist');
  });

  /****************************
  Создание таска очищения кеша
  1. Очищение кеша вручную
  *****************************/
  gulp.task("clear", function(){
  	return cache.clearAll();
  });

  /**********************************************
  Создание таска для отслеживания всех изменений
  1. Отслеживание изменений в less-файлах
  2. Отслеживание изменений в html-файлах
  3. Отслеживание изменений в js-файлах
  ***********************************************/
  gulp.task("watch", function(){
  	gulp.watch("app/less/**/*.less", gulp.series("less","styles"));
  	gulp.watch("app/css/**/*.css", sync.reload);
  	gulp.watch("app/**/*.html",sync.reload);
  	gulp.watch("app/js/**/*.js", sync.reload);
  });

  /*********************************************
  Создание таска подготовки файлов в продакшен
  1. Отправка шрифтов в продакшен
  2. Отправка js-файлов в продакшен\
  3. Отправка html-файлов в продакшен
  **********************************************/
  gulp.task("build-files", function(){

  	gulp.src("app/fonts/**/*")
  	.pipe(gulp.dest("dist/fonts"));

  	gulp.src("app/js/**/*")
  	.pipe(gulp.dest("dist/js"));

  	gulp.src("app/*.html")
  	.pipe(gulp.dest("dist"));
  });

  /******************************************************************
  Создание таска для инициирования генерации файлов для продакшена
  1. Вызов таска "img"
  2. Вызов таска "less"
  3. Вызов таска "styles"
  4. Вызов таска "scripts"
  5. Вызов таска "build-files"
  *******************************************************************/
  gulp.task("build",gulp.series(
  	gulp.parallel(
  		"img",
  		"less",
  		"styles",
  		"scripts",
  		"build-files"
  	))
  );

  /***************************************************
  Создание таска для запуска сервера
  1. Указание папки в качестве сервера
  2. Выключение уведомления о состоянии подключения
  3. Слежка за изменениями в папке
  ****************************************************/
  gulp.task("browse", function(){
  	sync({
  		server:{
  			baseDir: "./app"
  		},
  		notify: false
  	});
  	sync.watch("app", sync.reload);
  });

  /*******************************************************
  Модифицирование таска по умолчанию - команда "gulp"
  1. Запуск таска "scripts"
  2. Запуск таска "less"
  3. Запуск таска "styles"
  4. Запуск таска "browse"
  5. Запуск таска "watch"
  ********************************************************/
  gulp.task("default", gulp.series([
  	gulp.parallel('scripts', 'less'),
  	gulp.parallel('browse', 'watch')
  ]))
